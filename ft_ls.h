/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls.h                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/06/28 14:45:06 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/07/26 11:12:43 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
#define FT_LS_H

#include <stdbool.h>

typedef struct  s_ls_opt
{
    char    **targetv;
    int     targetc;
    int     terminal_width;
    bool    opt_long; // -l
    bool    opt_recursive; // -R
    bool    opt_all; // -a
    bool    opt_reverse_sort; // -r
    bool    opt_time_sort; // -t
}               t_ls_opt;

bool		ft_ls(struct s_ls_opt *options);

#endif
