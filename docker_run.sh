#!/bin/sh

set -eu

IMAGE_NAME="ls_build_env"
CONTAINER_NAME="ls_env"

init()
{
    if ! docker inspect ${CONTAINER_NAME} >> /dev/null; then
        echo "Creating ${CONTAINER_NAME}"
        ./docker_build.sh
    fi
}

run()
{
    docker exec \
           "${CONTAINER_NAME}" \
           ${@}
}

main()
{
    init
    run ${@}
}

main ${@}

exit 0
