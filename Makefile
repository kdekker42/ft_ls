# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    Makefile                                           :+:    :+:             #
#                                                      +:+                     #
#    By: kpereira <kpereira@student.codam.nl>         +#+                      #
#                                                    +#+                       #
#    Created: 2021/06/17 13:58:33 by kpereira      #+#    #+#                  #
#    Updated: 2021/06/17 13:58:33 by kpereira      ########   odam.nl          #
#                                                                              #
# **************************************************************************** #

NAME =		ft_ls
LIBLS =		ft_ls.a
SRC_DIR =	src/
FILES =		dir file ft_ls print_long print_multi_column sort target file_select
OBJS =		$(addprefix $(SRC_DIR), $(addsuffix .o, $(FILES)))
DBUGFLAGS =	-g
CC =		gcc
CFLAGS =	-Wall -Werror -Wextra
CINCLUDES = -I libft -I.

all: $(NAME)

$(NAME): $(OBJS)
	@make -C libft --no-print-directory
	@ar rc $(LIBLS) $(OBJS)
	@$(CC) $(CFLAGS) main.c -o $@ $(LIBLS) libft/libft.a $(CINCLUDES) -g 

#-fsanitize=leak -g
%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@ $(CINCLUDES) -g -fsanitize=leak -g

clean:
	@make -C libft clean --no-print-directory
	@rm -f $(OBJS)
	@rm -f $(LIBLS)

fclean: 
	@make -C libft fclean --no-print-directory
	@make clean --no-print-directory
	@rm -f $(NAME)

re:
	@make -C libft re --no-print-directory
	@make fclean --no-print-directory
	@make all --no-print-directory

pipe: $(OBJS)
	@ar rc $(LIBLS) $(OBJS)
	@$(CC) $(CFLAGS) main.c -o $(NAME) $(LIBLS) libft-pipe.a $(CINCLUDES)
	@./$(NAME)

test: $(OBJ_FILES)
	@make
	@./$(NAME)

.PHONY: all clean fclean re pipe test
