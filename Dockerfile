#
#	Docker file for ft ls
#

FROM alpine

RUN apk add --no-cache \
			clang \
			libc-dev \
			make \
			binutils \
			gcc \
			libuuid

ENTRYPOINT [ "" ]
