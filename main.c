/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   main.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/06/28 14:45:12 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/07/23 20:35:02 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"

#include <sys/ioctl.h>
#include <unistd.h>
#include <stdlib.h> 

static bool options(int argc, char **argv, t_ls_opt *ls_opt)
{
	struct winsize 	w;
	t_ft_getopt gopt = {
		.optind = 1,
		.opterr = 1,
		.optopt = 0,
		.optarg = NULL,
	};
	int				opt;

	ft_bzero(&w, sizeof(struct winsize));
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	ls_opt->terminal_width = w.ws_col; // 0 if ioctl() failed
	while (true)
	{
		opt = ft_getopt(&gopt, argc, argv, "lRart");
		if (opt == -1)
			break;
		switch (opt)
		{
		case 'l':
			ls_opt->opt_long = true;
			break;
		case 'R':
			ls_opt->opt_recursive = true;
			break;
		case 'a':
			ls_opt->opt_all = true;
			break;
		case 'r':
			ls_opt->opt_reverse_sort = true;
			break;
		case 't':
			ls_opt->opt_time_sort = true;
			break;
		default:
			ft_fprintf(STDERR_FILENO, "Usage: %s [-lRart] name\n", argv[0]);
			return false;
		}
	}
	ls_opt->targetv = argv + gopt.optind;
	ls_opt->targetc = argc - gopt.optind;
	return true;
}

// #define RED   "\x1B[31m"
// #define GRN   "\x1B[32m"
// #define YEL   "\x1B[33m"
// #define BLU   "\x1B[34m"
// #define MAG   "\x1B[35m"
// #define CYN   "\x1B[36m"
// #define WHT   "\x1B[37m"
// #define RESET "\x1B[0m"

int		main(int argc, char **argv)
{
	// ft_printf(RED"red\n"RESET);
	// ft_printf(GRN"green\n"RESET);
	// ft_printf(YEL"yellow\n"RESET);
	// ft_printf(BLU"blue\n"RESET);
	// ft_printf(MAG"magenta\n"RESET);
	// ft_printf(CYN"cyan\n"RESET);
	// ft_printf(WHT"white\n"RESET);
	char *dot_path;
	t_ls_opt ls_opt;

	dot_path = ".";
	ft_bzero(&ls_opt, sizeof(t_ls_opt));
	if (!options(argc, argv, &ls_opt))
		exit(EXIT_FAILURE);
	if (ls_opt.targetc == 0)
	{
		ls_opt.targetc = 1;
		ls_opt.targetv = &dot_path;
	}
	if (!ft_ls(&ls_opt))
		exit(EXIT_FAILURE);
	exit(EXIT_SUCCESS);
}
