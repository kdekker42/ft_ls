/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   pf_memcpy.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/04/17 11:05:25 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/06/24 18:11:32 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "_ft_printf.h"

char	*pf_memcpy(char *dst, char *src, size_t n)
{
	return ((char *)ft_memcpy((void *)dst, (const void *)src, n));
}
