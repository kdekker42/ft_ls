/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   _ft_get_next_line.h                                :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/06/24 17:51:09 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/06/24 19:14:18 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef _FT_GET_NEXT_LINE
#define _FT_GET_NEXT_LINE

#include "libft.h"
#include <unistd.h>

# define BUFF_SIZE 42

typedef struct			s_fd_list
{
	ssize_t				fd;
	ssize_t				buffer_usage;
	char				*buffer;
	struct s_fd_list	*next;
}						t_fd_list;

// int					ft_get_next_line(const int fd, char **line);

#endif
