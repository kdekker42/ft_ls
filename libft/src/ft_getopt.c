/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_getopt.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/06/24 20:14:07 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/06/28 13:31:47 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdbool.h>
#include <unistd.h>

static bool set_optarg(t_ft_getopt *opt, char **cur_char, int argc, char **argv)
{
	if (*(*cur_char + 1) != '\0') 
	{
		opt->optarg = *cur_char + 1;
	}
	else
	{
		if (opt->optind + 1 >= argc)
			return false;
		else
		{
			opt->optarg = argv[opt->optind + 1];
			opt->optind += 1;
		}
	}
	return true;
}

static void inc_cur_char(t_ft_getopt *opt, char **cur_char)
{
	*cur_char += 1;
	if (**cur_char == '\0')
	{
		opt->optind += 1;
	}
}

static int process(t_ft_getopt *opt, char **cur_char, int argc, char **argv, const char *optstring)
{
	if (!ft_isalnum(**cur_char))
		return -1;
	char *cur_in_opstring = ft_strchr(optstring, (int)**cur_char);
	if (cur_in_opstring == NULL)
	{
		ft_fprintf(STDERR_FILENO, "%s: invalid option -- '%c'\n", argv[0], **cur_char);
		opt->optopt = **cur_char;
		inc_cur_char(opt, cur_char);
		return '?';
	}
	else if (*(cur_in_opstring + 1) == ':')
	{
		if (!set_optarg(opt, cur_char, argc, argv))
		{
			ft_fprintf(STDERR_FILENO, "%s: option requires an argument -- '%c'\n", argv[0], **cur_char);
			opt->optopt = *cur_in_opstring;
			inc_cur_char(opt, cur_char);
			return '?';
		}
		else
		{
			*cur_char = NULL;
			opt->optind += 1;
		}
	}
	else
		inc_cur_char(opt, cur_char);
	return (int)*cur_in_opstring;
}

static bool set_cur_char_to_argv_optind(t_ft_getopt *opt, char **cur_char, int argc, char **argv)
{
	if (opt->optind >= argc)
		return false;
	*cur_char = argv[opt->optind];
	if (**cur_char != '-') 
		return false;
	*cur_char += 1;
	if (**cur_char == '\0')
		return false;
	return true;
}

int ft_getopt(t_ft_getopt *opt, int argc, char **argv, const char *optstring)
{
	static char *cur_char;

	opt->optarg = NULL;
	opt->optopt = 0;
	if (opt->optind >= argc)
		return -1;
	if (cur_char == NULL || *cur_char == '\0')
	{
		if (!set_cur_char_to_argv_optind(opt, &cur_char, argc, argv))
					return -1;
	}
	return process(opt, &cur_char, argc, argv, optstring);
}
