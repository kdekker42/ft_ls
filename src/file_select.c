/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   file_select.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/07/23 23:52:51 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/07/24 15:41:58 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "_ft_ls.h"

static int count_dir_files(t_directory *from)
{
	int res;
	int i;

	res = 0;
	i = 0;
	while(i < from->file_count)
	{
		if (S_ISDIR(from->files[i].file_stat.st_mode))
			res += 1;
		i += 1;
	}
	return res;
}

// count cant be 0
// {same comment as on get_target_files() in target.c}
static t_err select_dir_or_non_dir_files(t_directory *to, t_directory *from, int count, bool only_dir_files)
{
	t_err err;
	int i_to;
	int i_from;

	to->path = ft_strdup("Fake Folder");
	if (to->path == NULL)
		return malloc_fail;
	to->file_count = count;
	to->files = ft_calloc(to->file_count, sizeof(t_file));
	if (to->files == NULL)
	{
		ft_memdel((void **)to->path);
		return malloc_fail;
	}
	i_to = 0;
	i_from = 0;
	while (i_to < to->file_count)
	{
		if (S_ISDIR(from->files[i_from].file_stat.st_mode) == only_dir_files)
		{
			err = copy_file(&to->files[i_to], &from->files[i_from]);
			if (err != nil)
			{
				free_n_files_content(to->files, i_to);
				ft_memdel((void **)to->path);
				ft_memdel((void **)to->files);
				return err;
			}
			i_to += 1;
		}
		i_from += 1;
	}
	return nil;
}

// the split folders contina
// there needs to be at least one file.
t_err split_dir_files_from_files(t_directory *from, t_directory *only_dirs, t_directory *no_dirs)
{
	int dir_count;
	t_err err;

	if (from->file_count == 0)
		return error;
	ft_bzero(only_dirs, sizeof(t_directory));
	ft_bzero(no_dirs, sizeof(t_directory));
	dir_count = count_dir_files(from);
	if (dir_count > 0)
	{
		err = select_dir_or_non_dir_files(only_dirs, from, dir_count, true);
		if (err != nil)
			return err;
	}
	if (from->file_count - dir_count > 0)
	{
		err = select_dir_or_non_dir_files(no_dirs, from, from->file_count - dir_count, false);
		if (err != nil)
		{
			if (dir_count > 0)
				free_dir_content(only_dirs);
			return err;
		}
	}
	return nil;
}
