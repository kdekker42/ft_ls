/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   print_long.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: kpereira <kpereira@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/07/04 14:56:50 by kpereira      #+#    #+#                 */
/*   Updated: 2021/07/04 18:26:57 by kpereira      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "_ft_ls.h"

#define LONG_FIELDS_TO_PRINT		7

static int		file_type_letter(int mode)
{
    char    c;

	c = 0;
	if (S_ISDIR(mode))
		c ='d';
	else if (S_ISCHR(mode))
		c ='c';
	else if (S_ISBLK(mode))
		c ='b';
	else if (S_ISLNK(mode))
		c ='l';
	else if (S_ISFIFO(mode))
		c ='p';
	else if (S_ISSOCK(mode))
		c ='s';
	else if (S_ISREG(mode))
		c ='-';
    return(c);
}

static void		print_file_permissions(int mode)
{
    static const char *rwx[] = {"---", "--x", "-w-", "-wx", "r--", "r-x", "rw-", "rwx"};
    static char bits[11];

    bits[0] = file_type_letter(mode);
    ft_strcpy(&bits[1], rwx[(mode >> 6) & 7]);
    ft_strcpy(&bits[4], rwx[(mode >> 3) & 7]);
    ft_strcpy(&bits[7], rwx[(mode & 7)]);
    if (mode & S_ISUID)
        bits[3] = (mode & S_IXUSR) ? 's' : 'S';
    if (mode & S_ISGID)
        bits[6] = (mode & S_IXGRP) ? 's' : 'l';
    if (mode & S_ISVTX)
        bits[9] = (mode & S_IXOTH) ? 't' : 'T';
    bits[10] = '\0';
    ft_printf("%s ", bits);
}

static void		print_links(nlink_t nlink)
{
	ft_printf("%d ", nlink);
}

static void		print_id(struct stat file_stat)
{
	struct passwd	*u_id;
	struct group	*g_id;

	u_id = getpwuid(file_stat.st_uid);
	g_id = getgrgid(file_stat.st_gid);
	ft_printf("%s %s ", u_id->pw_name, g_id->gr_name);

}

static void		print_size(off_t size)
{
	ft_printf("%d ", size);
}

// static void		print_date()
// {

// }

static void		print_filename(char *path)
{
	char *res;

	res = ft_strrchr(path, '/');
	if (res == NULL)
		res = path;
	else
		res += 1;
    ft_printf("%s\n", res);
}

// static void default_max_len_values(int *max)
// {
// 	max[0] = 11;	//permissions
// 	max[1] = 0;		//number of links
// 	max[2] = 0;		//uid
// 	max[3] = 0;		//gio
// 	max[4] = 4;		//size
// 	max[5] = 12;	//date
// 	max[6] = 0;		//name
// }

// static void	get_padding_max_lens(t_directory *dir, int *max)
// {
// 	int len;
// 	int i;
// 	int	j;

// 	max = 0;
// 	i = 0;
// 	while (i < dir->file_count)
// 	{
// 		while (i < LONG_FIELDS_TO_PRINT)
// 		{
// 			len = ft_strlen((dir->files[i]));
// 			if (len > max)
// 				max = len;
// 			i += 1;
// 		}
// 		i++;
// 		j = 0;
// 	}
// }


bool    	print_long_list(t_directory *dir,  t_ls_opt *opt)
{

    int	padding[LONG_FIELDS_TO_PRINT];
	int i;

	i = 0;
    // get_padding_max_lens(dir, &padding);
	while (i < dir->file_count)
	{
		print_file_permissions(dir->files[i].file_stat.st_mode);
		print_links(dir->files[i].file_stat.st_nlink);
		print_id(dir->files[i].file_stat);
		print_size(dir->files[i].file_stat.st_size);
		print_filename(dir->files[i].path);
		i++;
	}
	
	(void)padding;
    (void)dir;
    (void)opt;
    return (true);
}
