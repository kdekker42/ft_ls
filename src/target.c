/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   target.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/07/23 20:37:16 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/07/26 11:14:11 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "_ft_ls.h"

static t_err read_file_at_path_to_dir_index(t_directory *dir, t_ls_opt *options, char *path, int file_index)
{
	t_err err;

	err = get_file(&dir->files[file_index], path, options);
	if (err != nil)
		return err;
	dir->files[file_index].path = ft_sprintf("%s", path);
	if (dir->files[file_index].path == NULL)
		return malloc_fail;
	dir->files[file_index].name = filename_from_path(dir->files[file_index].path);
	if (dir->files[file_index].name == NULL)
	{
		ft_memdel((void **)&dir->files[file_index].path);
		return malloc_fail;
	}
	return nil;
}

static t_err get_files_loop(t_directory *dir, t_ls_opt *options)
{
	t_err err;
	int target_index;
	int file_index;

	target_index = 0;
	file_index = 0;
	while (target_index < options->targetc)
	{
		err = read_file_at_path_to_dir_index(dir, options, options->targetv[target_index], file_index);
		if (err != nil)
		{
			if (err == malloc_fail)
				return malloc_fail;
			ft_printf("ft_ls: %s: No such file or directory\n", options->targetv[target_index]);
			target_index += 1;
			continue;
		}
		target_index += 1;
		file_index += 1;
	}
	dir->file_count = file_index;
	return nil;
}

// the files in this t_directory do not represent the contents of an actual directory.
// instead it will contain the files specified in the program arguments.
// it will free the same way as any other t_directory.
t_err get_target_files(t_directory *dir, t_ls_opt *options)
{
	t_err err;

	ft_bzero(dir, sizeof(t_directory));
	dir->path = ft_strdup("Fake Folder");
	if (dir->path == NULL)
		return malloc_fail;
	if (options->targetc == 0)
		return error;
	dir->files = ft_calloc(options->targetc, sizeof(t_file));
	if (dir->files == NULL)
	{
		ft_memdel((void **)dir->path);
		return malloc_fail;
	}
	err = get_files_loop(dir, options);
	if (err != nil)
	{
		ft_memdel((void **)dir->path);
		ft_memdel((void **)&dir->files);
		return err;
	}
	return nil;
}
