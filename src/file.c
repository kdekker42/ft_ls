/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   file.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/07/23 20:37:25 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/07/23 23:55:09 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "_ft_ls.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <stdbool.h>

// switch (direntp->d_type) {
//     case DT_REG:
//         ++count;
//         break;
//     case DT_DIR:            
//         npath=malloc(strlen(path)+strlen(direntp->d_name)+2);
//         sprintf(npath,"%s/%s",path, direntp->d_name);
//         count += countfiles(npath);
//         free(npath);
//         break;
// }

void free_n_files_content(t_file *f_array, int n)
{
	int i;

	i = 0;
	while(i < n)
	{
		free_file_content(&f_array[i]);
		i++;
	}	
}

char *filename_from_path(char *path)
{
	char *res;

	res = ft_strrchr(path, '/');
	if (res == NULL)
		res = path;
	else
		res += 1;
	return ft_sprintf("%s", res);
}

void free_file_content(t_file *file)
{
    ft_memdel((void **)&file->path);
    ft_memdel((void **)&file->name);
}   

t_err copy_file(t_file *to, t_file *file)
{
	to->nlink = file->nlink;
	to->size = file->nlink;
	to->file_stat = file->link_stat;
	if (file->path != NULL)
	{
		to->path = ft_sprintf("%s", file->path);
		if (to->path == NULL)
			return malloc_fail;
	}
	if (file->name != NULL)
	{
		to->name = ft_sprintf("%s", file->name);
		if (to->name == NULL)
		{
			ft_memdel((void **)&to->path);
			return malloc_fail;
		}
	}
	if (file->gid != NULL)
	{
		to->gid = ft_sprintf("%s", file->gid);
		if (to->gid == NULL)
		{
			ft_memdel((void **)&to->path);
			ft_memdel((void **)&to->name);
			return malloc_fail;
		}
	}
	if (file->uid != NULL)
	{
		to->uid = ft_sprintf("%s", file->uid);
		if (to->uid == NULL)
		{
			ft_memdel((void **)&to->path);
			ft_memdel((void **)&to->name);
			ft_memdel((void **)&to->gid);
			return malloc_fail;
		}
	}
	return nil;
}

t_err get_file(t_file *res, char *path, t_ls_opt *opt)
{
    (void)opt;
	ft_bzero(res, sizeof(res));
	res->path = path;
	if (stat(path, &res->file_stat) == -1)
		return error;
	if ((res->file_stat.st_mode & S_IFMT) == S_IFLNK)
		if (lstat(path, &res->link_stat) == -1)
			return error;
	return nil;
}
