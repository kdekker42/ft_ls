/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   sort.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/07/23 20:37:12 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/07/23 20:37:12 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */


#include "_ft_ls.h"

void sort_name(int filec, t_file *filev, bool reversed)
{
    int i;
    int j;
    t_file tmp;
    bool swap;

    i = 0;
    while (i < filec)
    {
        j = 0;
        while (j < filec - 1 - i)
        {
            swap = ft_strcmp(filev[j].name, filev[j + 1].name) > 0;
            if (reversed)
                swap = ft_strcmp(filev[j + 1].name, filev[j].name) > 0;
            if (swap)
            {
                tmp = filev[j + 1];
                filev[j + 1] = filev[j];
                filev[j] = tmp;
            }
            j++;
        }
        i++;
    }
}
