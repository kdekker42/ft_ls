/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/07/23 23:48:10 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/07/23 23:48:10 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "_ft_ls.h"

static void print_one_dir(t_directory *dir, t_ls_opt *options)
{
	if (options->opt_long)
		print_long_list(dir, options);
	else 
		print_multi_column(dir, options);
}

static t_err print_dir(t_file *dir_file, t_ls_opt *options)
{
	int i;
	t_err err;
	t_directory cur_folder;

	err = get_dir(&cur_folder, dir_file->path, options);
	if (err != nil)
		return err;
	sort_name(cur_folder.file_count, cur_folder.files, options->opt_reverse_sort);
	print_one_dir(&cur_folder, options);
	if (options->opt_recursive)
	{
		i = 0;
		while (i < cur_folder.file_count)
		{
			if (S_ISDIR(cur_folder.files[i].file_stat.st_mode))
			{
				ft_printf("\n");
				ft_printf("%s:\n", cur_folder.files[i].path);
				print_dir(&cur_folder.files[i], options);
			}
			i++;
		}
	}
	free_dir_content(&cur_folder);
	return nil;
}

static t_err print_dir_targets(t_directory *only_dirs, t_ls_opt *options, bool nothing_printed_previously)
{
	t_err err;
	int i;

	i = 0;
	while (i < only_dirs->file_count)
	{
		if (i > 0 || !nothing_printed_previously)
			ft_putstr("\n");
		if (only_dirs->file_count > 1 || !nothing_printed_previously)
			ft_printf("%s:\n", only_dirs->files[i].path);	
		err = print_dir(&only_dirs->files[i], options);
		if (err != nil)
			return err;
		i++;
	}
	return nil;
}

// add -m flag
bool ft_ls(t_ls_opt *options)
{
	t_err err;
	t_directory all_input;
	t_directory no_dirs;
	t_directory only_dirs;

	if (options->targetc == 0)
		return false;
	err = get_target_files(&all_input, options);
	if (err != nil)
		return err;
	sort_name(all_input.file_count, all_input.files, options->opt_reverse_sort);
	err = split_dir_files_from_files(&all_input, &only_dirs, &no_dirs);
	if (err != nil)
		return err;
	if (no_dirs.file_count > 0)
	{
		print_one_dir(&no_dirs, options);
		free_dir_content(&no_dirs);
	}
	if (only_dirs.file_count > 0)
	{
		print_dir_targets(&only_dirs, options, no_dirs.file_count == 0);
		free_dir_content(&only_dirs);
	}
	free_dir_content(&all_input);
	return true;
}
