/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   print_multi_column.c                               :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/07/02 13:15:47 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/07/26 11:13:12 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "_ft_ls.h"

static char *filename(t_file *file)
{
	return file->name;
}

static void file_len_stats_for_dir(t_directory *dir, t_filename_stats *fs)
{
	int len;
	int i;

	fs->max_len = 0;
	i = 0;
	while (i < dir->file_count)
	{
		len = ft_strlen(filename(&dir->files[i]));
		fs->total_len += len;
		if (len > fs->max_len)
			fs->max_len = len;
		i += 1;
	}
}

static int ceil_to_int(double nbr)
{
	int res;
	res = (int)nbr;
	if (nbr - res > 0.0)
		res += 1;
	return res;
}

static bool print_loop(t_directory *dir, t_ls_opt *opt, char *format, int padding)
{
	int columns;
	int rows;
	int	cur_col;
	int cur_row;
	int i;

	columns = opt->terminal_width / padding;
	rows = ceil_to_int((double)dir->file_count / columns);
	cur_col = 0;
	cur_row = 0;
	i = 0;
	while (true)
	{
		i = cur_col * rows + cur_row;
		if (i < dir->file_count)
			ft_printf(format, filename(&dir->files[i]));
		cur_col += 1;
		if (cur_col >= columns)
		{
			ft_putchar('\n');
			cur_col = 0;
			cur_row += 1;
			if (cur_row >= rows)
				break;
		}
	}
	return true;
}

bool print_multi_column(t_directory *dir, t_ls_opt *opt)
{
	int padding;
	char *format_str;
	int i;
	t_filename_stats fs;

	ft_bzero(&fs, sizeof(fs));
	file_len_stats_for_dir(dir, &fs);
	padding = fs.max_len + 1;
	format_str = ft_sprintf("%%-%ds", padding);
	if (format_str == NULL)
		return false;
	if (opt->terminal_width != 0)
		print_loop(dir, opt, format_str, padding);
	else {
		i = 0;
		while (i < dir->file_count)
		{
			ft_printf("%s\n", dir->files[i].name);
			i++;
		}
	}
	ft_memdel((void **)&format_str);
	return true;
}
