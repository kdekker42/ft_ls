/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   dir.c                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/07/23 20:37:31 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/07/23 20:38:56 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "_ft_ls.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <stdbool.h>

static int get_dir_size(char *path, t_ls_opt *opt)
{
	DIR             *dir_ptr;
	struct dirent   *direntp;
	int             count;

	dir_ptr = opendir(path);
	if (dir_ptr == NULL)
		return -1;
	count = 0;
	while (true)
	{
		direntp = readdir(dir_ptr);
		if (direntp == NULL) // EOF or error...
			break;
		if (!opt->opt_all && direntp->d_name[0] == '.') // also in read_dir()
			continue;
		count += 1;
	}
	closedir(dir_ptr);
	return count;
}

static char *path_add(char *path, char *add)
{
	int path_len;

	path_len = ft_strlen(path);
	if (path_len == 0)
		return ft_strdup(add);
	if (path[path_len - 1] == '/')
		return ft_sprintf("%s%s", path, add);
	return ft_sprintf("%s/%s", path, add);
}

static t_err read_dir(t_directory *dir, t_ls_opt *opt)
{
	DIR             *dir_ptr;
	struct dirent   *direntp;
	int             count;
	char            *file_path;
	t_err           err;

	dir_ptr = opendir(dir->path);
	if (dir_ptr == NULL)
		return error;
	count = 0;
	while (true)
	{
		direntp = readdir(dir_ptr);
		if (direntp == NULL) // EOF or error...
			break;
		if (!opt->opt_all && direntp->d_name[0] == '.') // also in get_dir_size()
			continue;
		file_path = path_add(dir->path, direntp->d_name);
		if (file_path == NULL)
		{
			free_n_files_content(dir->files, count);
			closedir(dir_ptr);
			return malloc_fail;
		}
		dir->files[count].name = ft_sprintf("%s", direntp->d_name);
		if (dir->files[count].name == NULL)
		{
			ft_memdel((void **)&file_path);
			free_n_files_content(dir->files, count);
			closedir(dir_ptr);
			return malloc_fail;
		}
		err = get_file(&dir->files[count], file_path, opt);
		if (err != nil)
		{
			ft_memdel((void **)&file_path);
			ft_memdel((void **)&dir->files[count].name);
			free_n_files_content(dir->files, count + 1);
			closedir(dir_ptr);
			return err;
		}
		count += 1;
	}
	closedir(dir_ptr);
	return nil;
}

void        free_dir_content(t_directory *dir)
{
	int i;

	i = 0;
	while (i < dir->file_count)
	{
		free_file_content(&dir->files[i]);
		i++;
	}
	ft_memdel((void **)&dir->path);
	ft_memdel((void **)&dir->files);
}

t_err get_dir(t_directory *dir, char *path, t_ls_opt *opt)
{
	t_err err;

	ft_bzero(dir, sizeof(t_directory));
	dir->file_count = get_dir_size(path, opt);
	if (dir->file_count == -1)
		return error;
	dir->path = ft_strdup(path);
	if (dir->path == NULL)
		return malloc_fail;
	dir->files = ft_calloc(dir->file_count, sizeof(t_file));
	if (dir->files == NULL)
	{
		ft_memdel((void **)&dir->path);
		return malloc_fail;
	}
	err = read_dir(dir, opt);
	if (err != nil)
	{
		ft_memdel((void **)&dir->path);
		ft_memdel((void **)&dir->files);
		return err;
	}
	return nil;
}
