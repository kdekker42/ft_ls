/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   _ft_ls.h                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: jandre-d <jandre-d@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/07/01 18:17:22 by jandre-d      #+#    #+#                 */
/*   Updated: 2021/07/01 18:17:22 by jandre-d      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef _FT_LS_H
#define _FT_LS_H

#include "libft.h"
#include "ft_ls.h"

#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>

typedef struct	s_filename_stats
{
	int			max_len;
	int64_t		total_len;
}				t_filename_stats;

typedef struct s_file {
	nlink_t		nlink;
	off_t		size;
	struct stat file_stat; // stats for the file
	struct stat link_stat; // if the file is a link, then this contains the stat for the link itself
	char		*path;
	char		*name;
	char		*gid;
	char		*uid;
}				t_file;

typedef struct s_directory {
	char        	*path;
	t_file			*files;
	int				file_count;
}				t_directory;

typedef enum e_err {
	nil,
	malloc_fail,
	error,
}			t_err;

void		free_dir_content(t_directory *dir);
t_err		get_dir(t_directory *dir, char *path, t_ls_opt *opt);
void		free_file_content(t_file *file);
t_err 		get_file(t_file *res, char *path, t_ls_opt *opt);
bool		print_multi_column(t_directory *dir, t_ls_opt *opt);
bool		print_long_list(t_directory *dir,  t_ls_opt *opt);
void 		sort_name(int filec, t_file *filev, bool reversed);
char 		*filename_from_path(char *path);
t_err 		get_target_files(t_directory *dir, t_ls_opt *options);
t_err split_dir_files_from_files(t_directory *from, t_directory *only_dirs, t_directory *no_dirs);
t_err copy_file(t_file *to, t_file *file);
void free_n_files_content(t_file *f_array, int n);

#endif
